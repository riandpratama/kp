@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Restok barang di gudang</div>
                
                <div class="panel-body">
					
					<div class="login-box-body col-md-3">
			            <p class="login-box-msg">Input Restok Barang di Gudang</p>
			            <form action="{{ route('barang-restok.store') }}" method="post" enctype="multipart/form-data">
			                @csrf
			                <div class="form-group has-feedback {{ $errors->has('tanggal') ? 'has-error' : '' }}">
	                    		<label>Tanggal</label>
			                    <input type="date" name="tanggal" class="form-control" required>
			                    <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
			                    @if ($errors->has('tanggal'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('tanggal') }}</strong>
			                        </span>
			                    @endif
			                </div>
	                    	<div class="form-group has-feedback {{ $errors->has('barang_1') ? 'has-error' : '' }}">
	                    		<label>PASSIVE SPLITER 1:2</label>
			                    <input type="number" name="barang_1" class="form-control" required value="0">
			                    <span class="glyphicon glyphicon-book form-control-feedback"></span>
			                    @if ($errors->has('barang_1'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('barang_1') }}</strong>
			                        </span>
			                    @endif
			                </div>
			                <div class="form-group has-feedback {{ $errors->has('barang_2') ? 'has-error' : '' }}">
			                	<label>PASSIVE SPLITER 1:4</label>
			                    <input type="number" name="barang_2" class="form-control" required value="0">
			                    <span class="glyphicon glyphicon-book form-control-feedback"></span>
			                    @if ($errors->has('barang_2'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('barang_2') }}</strong>
			                        </span>
			                    @endif
			                </div>
			                <div class="form-group has-feedback {{ $errors->has('barang_3') ? 'has-error' : '' }}">
			                	<label>PASSIVE SPLITER 1:8</label>
			                    <input type="number" name="barang_3" class="form-control" required value="0">
			                    <span class="glyphicon glyphicon-book form-control-feedback"></span>
			                    @if ($errors->has('barang_3'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('barang_3') }}</strong>
			                        </span>
			                    @endif
			                </div>
			                <div class="form-group has-feedback {{ $errors->has('barang_4') ? 'has-error' : '' }}">
			                	<label>PASSIVE SPLITER 1:16</label>
			                    <input type="number" name="barang_4" class="form-control" required value="0">
			                    <span class="glyphicon glyphicon-book form-control-feedback"></span>
			                    @if ($errors->has('barang_4'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('barang_4') }}</strong>
			                        </span>
			                    @endif
			                </div>
			                <div class="form-group has-feedback {{ $errors->has('file') ? 'has-error' : '' }}">
			                	<label>Upload BA Restok</label>
			                    <input type="file" name="file" class="form-control" required>
			                    <span class="glyphicon glyphicon-book form-control-feedback"></span>
			                    @if ($errors->has('file'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('file') }}</strong>
			                        </span>
			                    @endif
			                </div>

			                <button type="submit"
                                class="btn btn-primary btn-block btn-flat"> SIMPAN</button>
			            </form>
			        </div>

                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="datatables" >
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>PASSIVE SPLITER 1:2 </th>
                                    <th>PASSIVE SPLITER 1:4 </th>
                                    <th>PASSIVE SPLITER 1:8 </th>
                                    <th>PASSIVE SPLITER 1:16 </th>
                                    <th>Upload </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $item)
                                <tr>
                                	<td>{{ $loop->iteration }}</td>
                                    <td>{{ date('d/m/Y', strtotime($item->tanggal)) }}</td>
                                    <td>{{ $item->barang_1 }}</td>
                                    <td>{{ $item->barang_2 }}</td>
                                    <td>{{ $item->barang_3 }}</td>
                                    <td>{{ $item->barang_4 }}</td>
                                    <td><a href="{{ asset('storage/'.$item->file_upload_path) }}" download="">Download</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });
</script>

@endsection