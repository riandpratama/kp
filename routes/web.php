<?php

Route::get('/cache', function(){
	Artisan::call('config:cache');
});

// Auth::routes();

Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'] ,function(){

	Route::get('barang', 'BarangController@index');
	
	Route::get('barang-restok', 'BarangRestokController@index');
	Route::post('barang-restok/store', 'BarangRestokController@store')->name('barang-restok.store');

	Route::get('barang-pengambilan-teknisi', 'BarangPengambilanTeknisiController@index');
	Route::post('barang-pengambilan-teknisi/store', 'BarangPengambilanTeknisiController@store')->name('barang-pengambilan-teknisi.store');

	Route::get('laporan-pemakaian-barang', 'LaporanPemakaianBarangController@index');
	Route::post('laporan-pemakaian-barang/store', 'LaporanPemakaianBarangController@store')->name('laporan-pemakaian-barang.store');
	Route::get('laporan-pemakaian-barang/destroy/{id}', 'LaporanPemakaianBarangController@destroy')->name('laporan-pemakaian-barang.destroy');
	Route::get('stok-barang-dibawa-teknisi/export-pdf', 'LaporanPemakaianBarangController@pdf')->name('stok.barang.dibawa.teknisi.pdf');
	
	Route::get('stok-barang-dibawa-teknisi', 'StokBarangDibawaTeknisiController@index');
	Route::get('stok-barang-dibawa-teknisi/destroy/{id}', 'StokBarangDibawaTeknisiController@destroy')->name('stok.barang.dibawa.teknisi.destroy');
});
