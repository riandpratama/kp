<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BarangRestok extends Model
{
    protected $table = 'barang_restok';

    protected $guarded = [];
}
