<?php

namespace App\Http\Controllers;

use DB;
use Alert;
use Storage;
use Illuminate\Http\Request;
use App\Models\Barang;
use App\Models\BarangPengambilanTeknisi;

class BarangPengambilanTeknisiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = BarangPengambilanTeknisi::orderBy('created_at', 'desc')->get();

        $teknisi = DB::table('master_teknisi')->get();
        $barang = DB::table('master_barang')->get();

        return view('backend.barang-pengambilan-teknisi.index', compact('data', 'teknisi', 'barang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $barang = Barang::where('id', $request->master_barang_id);
        $barangJumlah = $barang->first()->jumlah;

        if (($barangJumlah - $request->jumlah) < 0 ) {
            return redirect()->back();
        }

        $barang->update([
            'jumlah' => $barangJumlah - $request->jumlah,
        ]);

        BarangPengambilanTeknisi::create([
            'tanggal' => $request->tanggal,
            'master_teknisi_id' => $request->master_teknisi_id,
            'master_barang_id' => $request->master_barang_id,
            'jumlah' => $request->jumlah,
        ]);

        Alert::success('Success!');

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $portfolio = Portfolio::findOrFail($id);

        if ($request->file('image') !== null && $request->file('image') !== ''){
            Storage::delete($portfolio->image);
        }
        if ($request->hasFile('image')) {
            $filename = $request->file('image')->store('uploads/portfolio');
        } else {
            $filename = $portfolio->image;
        }

        $portfolio->name = $request->name;
        $portfolio->filter = $request->filter;
        $portfolio->image = $filename;
        $portfolio->save();

        Alert::success('Success!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $portfolio = Portfolio::findOrFail($id);

        Storage::delete($portfolio->photo);
        $portfolio->delete();

        Alert::success('Success!');

        return redirect()->back();
    }
}
