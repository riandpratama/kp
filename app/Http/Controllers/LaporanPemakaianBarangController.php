<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use Alert;
use Session;
use App\Models\BarangPengambilanTeknisi;
use App\Models\LaporanPemakaianBarang;
use Illuminate\Http\Request;

class LaporanPemakaianBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = LaporanPemakaianBarang::orderBy('created_at', 'DESC')->get();
        $teknisi = DB::table('master_teknisi')->get();
        $barang = DB::table('master_barang')->get();

        return view('backend.laporan-pemakaian-barang.index', compact('data','teknisi','barang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = BarangPengambilanTeknisi::where('master_barang_id', $request->master_barang_id)
                    ->where('master_teknisi_id', $request->master_teknisi_id)->first();
        
        if (!isset($data)) {
            $jumlahData = 0;
        } else {
            $jumlahData = $data->jumlah;
        }

        if ($jumlahData == 0 ) {
            Session::flash('gagal');

            return redirect()->back();
        } else {
            BarangPengambilanTeknisi::where('master_barang_id', $request->master_barang_id)
                    ->where('master_teknisi_id', $request->master_teknisi_id)->update([
                        'jumlah' => $jumlahData - 1
                    ]);
        }

        if ($request->hasFile('foto_label_odp')) {
            $filename1 = $request->file('foto_label_odp')->store('uploads/foto_label_odp');
        } else {
            $filename1 = NULL;
        }

        if ($request->hasFile('foto_sebelum_dipasang')) {
            $filename2 = $request->file('foto_sebelum_dipasang')->store('uploads/foto_sebelum_dipasang');
        } else {
            $filename2 = NULL;
        }

        if ($request->hasFile('foto_sesudah_dipasang')) {
            $filename3 = $request->file('foto_sesudah_dipasang')->store('uploads/foto_sesudah_dipasang');
        } else {
            $filename3 = NULL;
        }

        LaporanPemakaianBarang::create([
            'tanggal' => $request->tanggal,
            'master_teknisi_id' => $request->master_teknisi_id,
            'master_barang_id' => $request->master_barang_id,
            'nomor_tiket' => $request->nomor_tiket,
            'nomor_label_odp' => $request->nomor_label_odp,
            'file_foto_label_odp' => $filename1,
            'file_foto_sebelum_dipasang' => $filename2,
            'file_foto_sesudah_dipasang' => $filename3,
        ]);

        Alert::success('Success!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = LaporanPemakaianBarang::findOrFail($id);

        $data->delete();

        Alert::success('Success!');

        return redirect()->back();
    }

    public function pdf()
    {
        $data = LaporanPemakaianBarang::orderBy('created_at', 'DESC')->get();
        
        $pdf = PDF::loadView('backend.laporan-pemakaian-barang.cetak', compact('data'))->setPaper('A4', 'portrait');
        return $pdf->stream('backend.laporan-pemakaian-barang.cetak', 'data');
    }
}
