<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BarangPengambilanTeknisi extends Model
{
    protected $table = 'barang_pengambilan_teknisi';

    protected $guarded = [];

    public function teknisi()
    {
    	return $this->belongsTo(Teknisi::class, 'master_teknisi_id')->withDefault();
    }

    public function barang()
    {
    	return $this->belongsTo(Barang::class, 'master_barang_id')->withDefault();
    }
}
