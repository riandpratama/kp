<?php

namespace App\Http\Controllers;

use Alert;
use Storage;
use App\Models\Barang;
use App\Models\BarangRestok;
use Illuminate\Http\Request;

class BarangRestokController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = BarangRestok::orderBy('created_at', 'desc')->get();

        return view('backend.barang-restok.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('file')) {
            $filename = $request->file('file')->store('uploads/restok');
        } else {
            $filename = NULL;
        }

        BarangRestok::create([
            'tanggal' => $request->tanggal,
            'barang_1' => $request->barang_1,
            'barang_2' => $request->barang_2,
            'barang_3' => $request->barang_3,
            'barang_4' => $request->barang_4,
            'file_upload_path' => $filename,
        ]);

        //Data 1
        $data1 = Barang::where('id',1)->first()->jumlah;
        Barang::where('id',1)->update([
            'jumlah'  => ($data1 + $request->barang_1)
        ]);

        //Data 2
        $data2 = Barang::where('id',2)->first()->jumlah;
        Barang::where('id',2)->update([
            'jumlah'  => ($data2 + $request->barang_2)
        ]);

        //Data 3
        $data3 = Barang::where('id',3)->first()->jumlah;
        Barang::where('id',3)->update([
            'jumlah'  => ($data3 + $request->barang_3)
        ]);

        //Data 4
        $data4 = Barang::where('id',4)->first()->jumlah;
        Barang::where('id',4)->update([
            'jumlah'  => ($data4 + $request->barang_4)
        ]);

        Alert::success('Success!');

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $photo = Photo::findOrFail($id);

        if ($request->file('photo') !== null && $request->file('photo') !== ''){
            Storage::delete($photo->photo);
        }
        if ($request->hasFile('photo')) {
            $filename = $request->file('photo')->store('uploads/photo');
        } else {
            $filename = $photo->photo;
        }

        $photo->name = $request->name;
        $photo->overlay = $request->overlay;
        $photo->facebook = $request->facebook;
        $photo->gmail = $request->gmail;
        $photo->linkedin = $request->linkedin;
        $photo->photo = $filename;
        $photo->save();

        Alert::success('Success!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $photo = Photo::findOrFail($id);

        Storage::delete($photo->photo);
        $photo->delete();

        Alert::success('Success!');

        return redirect()->back();
    }
}
