@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Laporan Pemakaian Barang</div>
                <div class="panel-body">
                	@if (Session::has('gagal'))
		                <div class="alert alert-danger alert-hidden" role="alert">
		                    <strong>Gagal, Maaf ada yang salah</strong>
		                </div>
		            @endif
                    <a href="" class="btn btn-primary" style="margin-bottom: 15px;" data-title="Tambah" data-toggle="modal" data-target="#tambah">
                    	<i class="fa fa-plus"></i>&nbsp;Tambah Data 
                    </a>
					<form action="{{ route('stok.barang.dibawa.teknisi.pdf') }}" method="get">
						<button type="submit" class="btn btn-success"><i class="fa fa-print"></i> Cetak PDF</button>
					</form>
					<br>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="data">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal </th>
                                    <th>Teknisi</th>
                                    <th>Pasif</th>
                                    <th>SC/IN </th>
                                    <th>Label ODP </th>
                                    <th>Foto Label ODP </th>
                                    <th>Foto Sebelum </th>
                                    <th>Foto Sesudah </th>
                                    <th>Delete </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $item)
                                <tr>
                                	<td>{{ $loop->iteration }}</td>
                                    <td>{{ date('d/m/Y', strtotime($item->tanggal)) }}</td>
                                    <td>{{ $item->teknisi->nama_teknisi }}</td>
                                    <td>{{ $item->barang->nama_barang }}</td>
                                    <td>{{ $item->nomor_tiket }}</td>
                                    <td>{{ $item->nomor_label_odp }}</td>
                                    <td><img src="{{ asset('storage/'.$item->file_foto_label_odp) }}" width="100"></td>
                                    <td><img src="{{ asset('storage/'.$item->file_foto_sebelum_dipasang) }}" width="100"></td>
                                    <td><img src="{{ asset('storage/'.$item->file_foto_sesudah_dipasang) }}" width="100"></td>
									<td>
    									<p data-placement="top" data-toggle="tooltip" title="Delete" style="display:inline-block;"> <form action="{{ route('laporan-pemakaian-barang.destroy', $item->id) }}" method="GET" style="display:inline-block;">
                                             <button title="Delete" class="btn btn-danger js-submit-confirm btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span> </button>
                                        </form>
                                    	</p>
                                    </td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambah" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
    	<div class="modal-content">
          <div class="modal-header">
          	<h4><i class="fa fa-plus"></i> Tambah data</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	      </div>
	          <div class="modal-body">
	          	<form action="{{ route('laporan-pemakaian-barang.store') }}" method="POST" enctype="multipart/form-data">
              		{{ csrf_field() }}
              		<div class="form-group{{ $errors->has('tanggal') ? ' has-error' : '' }}">
	                    <label for="tanggal">Tanggal</label>
	                    <input id="tanggal" type="date" class="form-control" name="tanggal" value="{{ old('tanggal') }}">
	                    @if ($errors->has('tanggal'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('tanggal') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group has-feedback {{ $errors->has('master_teknisi_id') ? 'has-error' : '' }}">
                		<label>Nama Teknisi</label>
	                    <select name="master_teknisi_id" id="master_teknisi_id" class="form-control">
	                    	@foreach($teknisi as $item)
	                    	<option value="{{ $item->id }}">{{ $item->nama_teknisi }}</option>
	                    	@endforeach
	                    </select>
	                    <span class="form-control-feedback"></span>
	                    @if ($errors->has('master_teknisi_id'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('master_teknisi_id') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group has-feedback {{ $errors->has('master_barang_id') ? 'has-error' : '' }}">
	                	<label>Barang (Jenis Pasif)</label>
	                    <select name="master_barang_id" id="master_barang_id" class="form-control">
	                    	@foreach($barang as $item)
	                    	<option value="{{ $item->id }}">{{ $item->nama_barang }}</option>
	                    	@endforeach
	                    </select>
	                    <span class="form-control-feedback"></span>
	                    @if ($errors->has('master_barang_id'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('master_barang_id') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('nomor_tiket') ? ' has-error' : '' }}">
	                    <label for="nomor_tiket">Nomor Tiket(SC/IN)</label>
	                    <input id="nomor_tiket" type="text" class="form-control" name="nomor_tiket" value="{{ old('nomor_tiket') }}">
	                </div>
	                <div class="form-group{{ $errors->has('nomor_label_odp') ? ' has-error' : '' }}">
	                    <label for="nomor_label_odp">Nomor Label ODP</label>
	                    <input id="nomor_label_odp" type="text" class="form-control" name="nomor_label_odp" value="{{ old('nomor_label_odp') }}">
	                </div>
	                <div class="form-group has-feedback {{ $errors->has('foto_label_odp') ? 'has-error' : '' }}">
	                	<label>Foto Label ODP</label>
	                    <input type="file" name="foto_label_odp" class="form-control" required>
	                    <span class="glyphicon glyphicon-book form-control-feedback"></span>
	                    @if ($errors->has('foto_label_odp'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('foto_label_odp') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group has-feedback {{ $errors->has('foto_sebelum_dipasang') ? 'has-error' : '' }}">
	                	<label>Foto Sebelum Dipasang</label>
	                    <input type="file" name="foto_sebelum_dipasang" class="form-control" required>
	                    <span class="glyphicon glyphicon-book form-control-feedback"></span>
	                    @if ($errors->has('foto_sebelum_dipasang'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('foto_sebelum_dipasang') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group has-feedback {{ $errors->has('foto_sesudah_dipasang') ? 'has-error' : '' }}">
	                	<label>Foto Sesudah Dipasang</label>
	                    <input type="file" name="foto_sesudah_dipasang" class="form-control" required>
	                    <span class="glyphicon glyphicon-book form-control-feedback"></span>
	                    @if ($errors->has('foto_sesudah_dipasang'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('foto_sesudah_dipasang') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="modal-footer">
	        			<button type="submit" class="btn btn-primary" onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span class="glyphicon glyphicon-check"></span> Simpan</button>
	        			<button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
	      	  		</div> 
	      	  	</form>
	      	  </div>
        </div>
    <!-- /.modal-content --> 
  	</div>
      <!-- /.modal-dialog modal-dialog-centered --> 
</div>

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
    	<div class="modal-content">
          <div class="modal-header">
          	<h4><i class="fa fa-edit"></i> Ubah data</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	      </div>
	          <div class="modal-body">
	          	<form role="form" method="POST" id="formEdit" enctype="multipart/form-data">
	                {{ csrf_field() }}
	                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
	                    <label for="title">Tittle</label>
	                    <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}">
	                    @if ($errors->has('title'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('title') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('description_main') ? ' has-error' : '' }}">
	                    <label for="description_main">Deskripsi Utama</label>
	                    <input id="description_main" type="text" class="form-control" name="description_main" value="{{ old('description_main') }}">
	                    @if ($errors->has('description_main'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('description_main') }}</strong>
	                        </span>
	                    @endif
	                </div>
              		<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
	                    <label for="description">Deskripsi Detail</label>
	                    <textarea name="description" id="description" cols="50" rows="20"></textarea>
	                    @if ($errors->has('description'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('description') }}</strong>
	                        </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }}">
	                    <label for="icon">Icon</label>
	                    <input id="icon" type="text" class="form-control" name="icon" value="{{ old('icon') }}" placeholder="ion-ios-paper-outline">
	                </div>
	                <div class="modal-footer">
	        			<button type="submit" class="btn btn-warning btn-lg" onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
	        			<button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
	      	  		</div>  
	      	  </div>
	      	</form>
        </div>
    <!-- /.modal-content --> 
  	</div>
      <!-- /.modal-dialog modal-dialog-centered --> 
</div>

@endsection

@section('js')
<script type="text/javascript">
  	$(document).ready(function() {
	    $('#data').dataTable();
	} );

	function insertFormEdit(button){
        var item = $(button).data('item');
        console.log(item);
        console.log($('#formEdit .form-group #facebook_edit'));
        $('form#formEdit').attr('action','{{ url("admin/service") }}/'+item.id+'/update');
        $('#formEdit .form-group #title').val(item.title);
        $('#formEdit .form-group #description_main').val(item.description_main);
        $('#formEdit .form-group #description').val(item.description);
        $('#formEdit .form-group #icon').val(item.icon);
    }
  </script>
  <script src="{{ asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
  <script src="{{ asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
  <script>
    $('textarea').ckeditor({
        "filebrowserImageBrowseUrl" : '/laravel-filemanager?type=Images',
        "filebrowserBrowseUrl" : "/laravel-filemanager?type=Files"
    });
  </script>
@endsection