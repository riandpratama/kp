import Vue from 'vue';
import VueRouter from 'vue-router';

import MainComponent from '@/js/components/Main';
import BlogComponent from '@/js/components/Blog';

Vue.use(VueRouter);

const router = new VueRouter({
	mode: 'history',
	routes: [
		{
			path: '/',
			name: 'main',
			component: MainComponent
		},
		{
			path: '/',
			name: 'blog',
			component: Blog
		}
	]
});

export default router;