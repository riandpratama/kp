@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Barang Pengambilan Teknisi</div>
                
                <div class="panel-body">
					
					<div class="login-box-body col-md-3">
			            <p class="login-box-msg">Input Barang Pengambilan Teknisi</p>
			            <form action="{{ route('barang-pengambilan-teknisi.store') }}" method="post" enctype="multipart/form-data">
			                @csrf
			                <div class="form-group has-feedback {{ $errors->has('tanggal') ? 'has-error' : '' }}">
	                    		<label>Tanggal</label>
			                    <input type="date" name="tanggal" class="form-control" required>
			                    <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
			                    @if ($errors->has('tanggal'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('tanggal') }}</strong>
			                        </span>
			                    @endif
			                </div>
	                    	<div class="form-group has-feedback {{ $errors->has('master_teknisi_id') ? 'has-error' : '' }}">
	                    		<label>Nama Teknisi</label>
			                    <select name="master_teknisi_id" id="master_teknisi_id" class="form-control">
			                    	@foreach($teknisi as $item)
			                    	<option value="{{ $item->id }}">{{ $item->nama_teknisi }}</option>
			                    	@endforeach
			                    </select>
			                    <span class="form-control-feedback"></span>
			                    @if ($errors->has('master_teknisi_id'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('master_teknisi_id') }}</strong>
			                        </span>
			                    @endif
			                </div>
			                <div class="form-group has-feedback {{ $errors->has('master_barang_id') ? 'has-error' : '' }}">
			                	<label>Barang (Jenis Pasif)</label>
			                    <select name="master_barang_id" id="master_barang_id" class="form-control">
			                    	@foreach($barang as $item)
			                    	<option value="{{ $item->id }}">{{ $item->nama_barang }}</option>
			                    	@endforeach
			                    </select>
			                    <span class="form-control-feedback"></span>
			                    @if ($errors->has('master_barang_id'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('master_barang_id') }}</strong>
			                        </span>
			                    @endif
			                </div>
			                <div class="form-group has-feedback {{ $errors->has('jumlah') ? 'has-error' : '' }}">
			                	<label>Jumlah</label>
			                    <input type="number" name="jumlah" class="form-control" required value="0">
			                    <span class="form-control-feedback"></span>
			                    @if ($errors->has('jumlah'))
			                        <span class="help-block">
			                            <strong>{{ $errors->first('jumlah') }}</strong>
			                        </span>
			                    @endif
			                </div>

			                <button type="submit"
                                class="btn btn-primary btn-block btn-flat"> SIMPAN</button>
			            </form>
			        </div>

                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="datatables" >
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Nama Teknisi</th>
                                    <th>Barang (Jenis Pasif)</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $item)
                                <tr>
                                	<td>{{ $loop->iteration }}</td>
                                    <td>{{ date('d/m/Y', strtotime($item->tanggal)) }}</td>
                                    <td>{{ $item->teknisi->nama_teknisi }}</td>
                                    <td>{{ $item->barang->nama_barang }}</td>
                                    <td>{{ $item->jumlah }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });
</script>

@endsection