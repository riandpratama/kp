<table cellpadding="10" border="1" style="text-align: center; font-size: 14px;">
	<tr>
		<td><b>No</b></td>
		<td><b>Teknisi</b></td>
		<td><b>Pasif</b></td>
		<td><b>SC/IN</b></td>
		<td><b>Label ODP</b></td>
		<td><b>Foto Label ODP</b></td>
		<td><b>Foto Sebelum</b></td>
		<td><b>Foto Sesudah</b></td>
	</tr>
	@foreach($data as $item)
	<tr>
		<td>{{ $loop->iteration }}.</td>
		<td>{{ $item->teknisi->nama_teknisi }}</td>
		<td>{{ $item->barang->nama_barang }}</td>
		<td>{{ $item->nomor_tiket }}</td>
		<td>{{ $item->nomor_label_odp }}</td>
		<td><img src="{{ asset('storage/'.$item->file_foto_label_odp) }}" height="60px"></td>
		<td><img src="{{ asset('storage/'.$item->file_foto_sebelum_dipasang) }}" height="60px"></td>
		<td><img src="{{ asset('storage/'.$item->file_foto_sesudah_dipasang) }}" height="60px"></td>
	</tr>
	@endforeach
</table>