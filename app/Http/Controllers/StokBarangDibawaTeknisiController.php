<?php

namespace App\Http\Controllers;

use DB;
use Alert;
use Storage;
use App\Models\Teknisi;
use App\Models\BarangPengambilanTeknisi;
use Illuminate\Http\Request;

class StokBarangDibawaTeknisiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('barang_pengambilan_teknisi as one')
                ->selectRaw('one.master_teknisi_id, one.master_barang_id, SUM(one.jumlah) as jumlah, master_teknisi.nama_teknisi, master_teknisi.nama_teknisi, master_barang.nama_barang')
                ->join('master_teknisi', 'one.master_teknisi_id', 'master_teknisi.id')
                ->join('master_barang', 'one.master_barang_id', 'master_barang.id')
                ->groupBy('master_teknisi_id', 'master_barang_id')
                ->orderBy('master_teknisi_id', 'asc')
                ->orderBy('master_barang_id', 'asc')
                ->get();

        return view('backend.stok-barang-dibawa-teknisi.index', compact('data'));
    }
}
