@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Stok Barang Yang Dibawa Teknisi</div>
                
                <div class="panel-body">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="data">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Teknisi </th>
                                    <th>Nama Barang</th>
                                    <th>Jumlah </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $item)
                                <tr>
                                	<td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->nama_teknisi }}</td>
                                    <td>{{ $item->nama_barang }}</td>
                                    <td>{{ $item->jumlah }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
  	$(document).ready(function() {
	    $('#data').dataTable();
	} );

	function insertFormEdit(button){
        var item = $(button).data('item');
        console.log(item);
        console.log($('#formEdit .form-group #facebook_edit'));
        $('form#formEdit').attr('action','{{ url("admin/testimonial") }}/'+item.id+'/update');
        $('#formEdit .form-group #name').val(item.name);
        $('#formEdit .form-group #status').val(item.status);
        $('#formEdit .form-group #message').val(item.message);
    }
  </script>
@endsection