<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LaporanPemakaianBarang extends Model
{
    protected $table = 'laporan_pemakaian_barang';

    protected $guarded = [];

    public function teknisi()
    {
    	return $this->belongsTo(Teknisi::class, 'master_teknisi_id')->withDefault();
    }

    public function barang()
    {
    	return $this->belongsTo(Barang::class, 'master_barang_id')->withDefault();
    }
}
