<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrator Alphacreativee',
            'email' => 'admin@alphacreativee.com',
            'password' => bcrypt('administrator007'),
            'created_at' => Carbon::now()->format('Y-m-d H:m:s'),
        	'updated_at' => Carbon::now()->format('Y-m-d H:m:s')
        ]);
    }
}
